package com.electrolux.appliance.dao.impl;

import com.electrolux.appliance.dao.ApplianceTypeDao;
import com.electrolux.appliance.dao.mapper.ApplianceTypeRecordMapper;
import com.electrolux.appliance.dto.appliancetype.CreateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.SelectApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRs;
import com.electrolux.appliance.model.ApplianceTypeModel;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.List;

import static jooq.tables.ApplianceType.APPLIANCE_TYPE;

@Repository
@RequiredArgsConstructor
public class ApplianceTypeDaoImpl implements ApplianceTypeDao {

    private static final String SUCCESS_MESSAGE = "Update appliance success!";
    private static final String ERROR_MESSAGE = "Update appliance failed!";

    private final ApplianceTypeRecordMapper mapper;
    private final DSLContext dsl;

    @Override
    public Long createApplianceType(CreateApplianceTypeRq rq) {
        try{
            return dsl.insertInto(APPLIANCE_TYPE, APPLIANCE_TYPE.TYPE_NAME)
                    .values(rq.getTypeName())
                    .returning()
                    .fetchOne().getTypeId();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public UpdateApplianceTypeRs updateApplianceType(UpdateApplianceTypeRq rq) {
        try {
            dsl.update(APPLIANCE_TYPE)
                    .set(APPLIANCE_TYPE.TYPE_NAME, rq.getTypeName())
                    .where(APPLIANCE_TYPE.TYPE_ID.eq(rq.getTypeId()))
                    .execute();
            return new UpdateApplianceTypeRs(true, SUCCESS_MESSAGE);
        } catch (Exception ex) {
            return new UpdateApplianceTypeRs(false, ERROR_MESSAGE);
        }
    }

    @Override
    public List<ApplianceTypeModel> selectApplianceType(SelectApplianceTypeRq rq) {
        return dsl.selectFrom(APPLIANCE_TYPE)
                .where(APPLIANCE_TYPE.TYPE_NAME.eq(rq.getTypeName()))
                .fetch()
                .map(mapper::map);
    }
}
