package com.electrolux.appliance.exceptions;

public class InvalidUpdateApplianceTypeRqException extends RuntimeException {

    public InvalidUpdateApplianceTypeRqException(String message) {
        super(message);
    }
}
