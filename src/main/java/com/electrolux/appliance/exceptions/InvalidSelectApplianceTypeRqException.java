package com.electrolux.appliance.exceptions;

public class InvalidSelectApplianceTypeRqException extends RuntimeException {

    public InvalidSelectApplianceTypeRqException(String message) {
        super(message);
    }
}
