package com.electrolux.appliance.controller;

import com.electrolux.appliance.dto.appliance.*;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidSelectApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceRqException;
import com.electrolux.appliance.service.CrudApplianceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ApplianceControllerTest {

    private static final String RS_MESSAGE = "rsMessage";
    private static final Long APPLIANCE_ID = 1L;
    private static final String APPLIANCE_NAME = "applianceName";
    private static final Double PRICE = 100D;
    private static final Long TYPE_ID = 1L;

    @Mock
    private CrudApplianceService applianceService;

    private CreateApplianceRq createApplianceRq;
    private UpdateApplianceRq updateApplianceRq;
    private SelectApplianceRq selectApplianceRq;
    private SelectApplianceByIdRq selectApplianceByIdRq;

    private ApplianceController sut;

    @BeforeEach
    void setUp() {
        initMocks(this);
        sut = new ApplianceController(applianceService);
    }

    @Test
    public void shouldCallApplianceService_whenCreateNewAppliance() {
        withValidCreateRq();
        sut.createAppliance(createApplianceRq);
        verify(applianceService).createAppliance(argThat(arg -> arg.equals(createApplianceRq)));
    }

    @Test
    public void shouldCallApplianceService_whenUpdateAppliance() {
        withValidUpdateRq();
        when(applianceService.updateApplience(updateApplianceRq)).thenReturn(getUpdateApplianceRs());
        sut.updateAppliance(updateApplianceRq);
        verify(applianceService).updateApplience(argThat(arg -> arg.equals(updateApplianceRq)));
    }

    @Test
    public void shouldReturnOkResponse_whenValidCreateRq() {
        withValidCreateRq();
        when(applianceService.createAppliance(createApplianceRq)).thenReturn(new CreateApplianceRs(1L));
        var rs = sut.createAppliance(createApplianceRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnOkResponse_whenValidUpdateRq() {
        withValidUpdateRq();
        when(applianceService.updateApplience(updateApplianceRq)).thenReturn(getUpdateApplianceRs());
        var rs = sut.updateAppliance(updateApplianceRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnOkResponse_whenValidSelectRq() {
        withValidSelectRq();
        when(applianceService.selectAppliance(selectApplianceRq)).thenReturn(getSelectApplianceRs());
        var rs = sut.selectAppliances(selectApplianceRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnOkResponse_whenValidSelectByIdRq() {
        withValidSelectIdRq();
        when(applianceService.selectAppliance(selectApplianceByIdRq)).thenReturn(getSelectApplianceRs());
        var rs = sut.selectAppliances(selectApplianceByIdRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnBadResponse_whenInvalidCreateRq() {
        withInvalidCreateRq();
        when(applianceService.createAppliance(createApplianceRq)).thenThrow(InvalidCreateApplianceRqException.class);
        var rs = sut.createAppliance(createApplianceRq);
        assertFalse(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnBadResponse_whenInvalidUpdateRq() {
        withInvalidUpdateRq();
        when(applianceService.updateApplience(updateApplianceRq)).thenThrow(InvalidUpdateApplianceRqException.class);
        var rs = sut.updateAppliance(updateApplianceRq);
        assertFalse(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnBadResponse_whenInValidSelectRq() {
        withInValidSelectRq();
        when(applianceService.selectAppliance(selectApplianceRq)).thenThrow(InvalidSelectApplianceRqException.class);
        var rs = sut.selectAppliances(selectApplianceRq);
        assertFalse(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnBadResponse_whenInValidSelectByIdRq() {
        withInValidSelectIdRq();
        when(applianceService.selectAppliance(selectApplianceByIdRq)).thenThrow(InvalidSelectApplianceRqException.class);
        var rs = sut.selectAppliances(selectApplianceByIdRq);
        assertFalse(rs.getBody().isSuccess());
    }

    private void withValidCreateRq() {
        createApplianceRq = new CreateApplianceRq(APPLIANCE_NAME, PRICE, TYPE_ID);
    }

    private void withValidUpdateRq() {
        updateApplianceRq = new UpdateApplianceRq(APPLIANCE_ID, APPLIANCE_NAME, PRICE, TYPE_ID);
    }

    private void withValidSelectRq() {
        selectApplianceRq = new SelectApplianceRq(APPLIANCE_NAME, PRICE, TYPE_ID);
    }

    private void withValidSelectIdRq() {
        selectApplianceByIdRq = new SelectApplianceByIdRq(asList(1L, 2L));
    }

    private void withInvalidCreateRq() {
        createApplianceRq = new CreateApplianceRq(null, null, null);
    }

    private void withInvalidUpdateRq() {
        updateApplianceRq = new UpdateApplianceRq(null, null, null, null);
    }

    private void withInValidSelectRq() {
        selectApplianceRq = new SelectApplianceRq(null, PRICE, TYPE_ID);
    }

    private void withInValidSelectIdRq() {
        selectApplianceByIdRq = new SelectApplianceByIdRq(null);
    }

    private UpdateApplianceRs getUpdateApplianceRs() {
        return new UpdateApplianceRs(true, RS_MESSAGE);
    }

    private SelectApplianceRs getSelectApplianceRs() {
        return new SelectApplianceRs(true, Collections.emptyList());
    }
}