package com.electrolux.appliance.service;

import com.electrolux.appliance.dto.appliancetype.*;

public interface CrudApplianceTypeService {

    CreateApplianceTypeRs createApplianceType(CreateApplianceTypeRq rq);

    UpdateApplianceTypeRs updateApplianceType(UpdateApplianceTypeRq rq);

    SelectApplianceTypeRs selectApplianceType(SelectApplianceTypeRq rq);
}
