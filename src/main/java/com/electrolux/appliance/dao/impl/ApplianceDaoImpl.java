package com.electrolux.appliance.dao.impl;

import com.electrolux.appliance.dao.ApplianceDao;
import com.electrolux.appliance.dao.creators.SelectConditionCreator;
import com.electrolux.appliance.dao.mapper.ApplianceRecordMapper;
import com.electrolux.appliance.dto.appliance.*;
import com.electrolux.appliance.model.ApplianceModel;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

import static jooq.tables.Appliance.APPLIANCE;

@Repository
@RequiredArgsConstructor
public class ApplianceDaoImpl implements ApplianceDao {

    private static final String SUCCESS_MESSAGE = "Update appliance success!";
    private static final String ERROR_MESSAGE = "Update appliance failed!";

    private final SelectConditionCreator conditionCreator;
    private final ApplianceRecordMapper mapper;
    private final DSLContext dsl;

    @Override
    public Long createAppliance(CreateApplianceRq rq) {
        try {
            return dsl.insertInto(APPLIANCE, APPLIANCE.APPLIANCE_NAME, APPLIANCE.PRICE, APPLIANCE.TYPE_ID)
                    .values(rq.getApplianceName(), new BigDecimal(rq.getPrice()), rq.getTypeId())
                    .returning()
                    .fetchOne()
                    .get(APPLIANCE.APPLIANCE_ID);
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public UpdateApplianceRs updateAppliance(UpdateApplianceRq rq) {
        try {
            dsl.update(APPLIANCE)
                    .set(APPLIANCE.APPLIANCE_NAME, rq.getApplianceName())
                    .set(APPLIANCE.PRICE, new BigDecimal(rq.getPrice()))
                    .set(APPLIANCE.TYPE_ID, rq.getTypeId())
                    .where(APPLIANCE.APPLIANCE_ID.eq(rq.getApplianceId()))
                    .execute();
            return new UpdateApplianceRs(true, SUCCESS_MESSAGE);
        } catch (Exception ex) {
            return new UpdateApplianceRs(false, ERROR_MESSAGE);
        }
    }

    @Override
    public List<ApplianceModel> selectAppliance(SelectApplianceRq rq) {
        return dsl.selectFrom(APPLIANCE)
                .where(conditionCreator.createConditionWhere(rq))
                .fetch()
                .map(m -> mapper.map(m));
    }

    @Override
    public List<ApplianceModel> selectAppliance(SelectApplianceByIdRq rq) {
        return dsl.selectFrom(APPLIANCE)
                .where(conditionCreator.createConditionIdIn(rq))
                .fetch()
                .map(mapper::map);
    }
}
