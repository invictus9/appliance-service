package com.electrolux.appliance.service.validator;

import com.electrolux.appliance.dto.appliancetype.CreateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.SelectApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRq;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceTypeRqException;
import com.electrolux.appliance.exceptions.InvalidSelectApplianceTypeRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceTypeRqException;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.isEmpty;

@Component
public class ApplianceTypeValidatorImpl implements ApplianceTypeValidator {

    private static final String INVALID_CREATE_APPLIANCE_TYPE_MESSAGE = "Invalid parameters to create new appliance type.";
    private static final String INVALID_UPDATE_APPLIANCE_TYPE_MESSAGE = "Invalid parameters to update appliance type.";
    private static final String INVALID_SELECT_APPLIANCE_TYPE_MESSAGE = "Invalid parameters to select appliance.";

    @Override
    public void valid(CreateApplianceTypeRq rq) {
        if (isEmpty(rq.getTypeName())) {
            throw new InvalidCreateApplianceTypeRqException(INVALID_CREATE_APPLIANCE_TYPE_MESSAGE);
        }
    }

    @Override
    public void valid(UpdateApplianceTypeRq rq) {
        if (isEmpty(rq.getTypeName())
                || rq.getTypeId() == null) {
            throw new InvalidUpdateApplianceTypeRqException(INVALID_UPDATE_APPLIANCE_TYPE_MESSAGE);
        }
    }

    @Override
    public void valid(SelectApplianceTypeRq rq) {
        if (isEmpty(rq.getTypeName())) {
            throw new InvalidSelectApplianceTypeRqException(INVALID_SELECT_APPLIANCE_TYPE_MESSAGE);
        }
    }
}
