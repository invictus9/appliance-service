package com.electrolux.appliance.controller;

import com.electrolux.appliance.dto.appliancetype.*;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceTypeRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceTypeRqException;
import com.electrolux.appliance.service.CrudApplianceTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ApplianceTypeControllerTest {

    private static final String RS_MESSAGE = "rsMessage";
    private static String TYPE_NAME = "typename";
    private static final Long TYPE_ID = 1L;

    @Mock
    private CrudApplianceTypeService applianceTypeService;

    private CreateApplianceTypeRq createApplianceTypeRq;
    private UpdateApplianceTypeRq updateApplianceTypeRq;
    private SelectApplianceTypeRq selectApplianceTypeRq;

    private ApplianceTypeController sut;

    @BeforeEach
    void setUp() {
        initMocks(this);
        sut = new ApplianceTypeController(applianceTypeService);
    }

    @Test
    public void shouldCallApplianceTypeService_whenCreateNewApplianceType() {
        withValidCreateTypeRq();
        sut.createApplianceType(createApplianceTypeRq);
        verify(applianceTypeService).createApplianceType(argThat(arg -> arg.equals(createApplianceTypeRq)));
    }

    @Test
    public void shouldCallApplianceTypeService_whenUpdateApplianceType() {
        withValidUpdateTypeRq();
        when(applianceTypeService.updateApplianceType(updateApplianceTypeRq)).thenReturn(getUpdateApplianceTypeRs());
        sut.updateApplianceType(updateApplianceTypeRq);
        verify(applianceTypeService).updateApplianceType(argThat(arg -> arg.equals(updateApplianceTypeRq)));
    }

    @Test
    public void shouldReturnOkResponse_whenValidCreateRq() {
        withValidCreateTypeRq();
        when(applianceTypeService.createApplianceType(createApplianceTypeRq)).thenReturn(new CreateApplianceTypeRs(1L));
        var rs = sut.createApplianceType(createApplianceTypeRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnOkResponse_whenValidUpdateRq() {
        withValidUpdateTypeRq();
        when(applianceTypeService.updateApplianceType(updateApplianceTypeRq)).thenReturn(getUpdateApplianceTypeRs());
        var rs = sut.updateApplianceType(updateApplianceTypeRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnOkResponse_whenValidSelectRq() {
        withValidSelectTypeRq();
        when(applianceTypeService.selectApplianceType(selectApplianceTypeRq)).thenReturn(getSelectApplianceTypeRs());
        var rs = sut.selectApplianceType(selectApplianceTypeRq);
        assertTrue(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnBadResponse_whenInvalidCreateRq() {
        withInvalidCreateRq();
        when(applianceTypeService.createApplianceType(createApplianceTypeRq)).thenThrow(InvalidCreateApplianceTypeRqException.class);
        var rs = sut.createApplianceType(createApplianceTypeRq);
        assertFalse(rs.getBody().isSuccess());
    }

    @Test
    public void shouldReturnBadResponse_whenInvalidUpdateRq() {
        withInvalidUpdateRq();
        when(applianceTypeService.updateApplianceType(updateApplianceTypeRq)).thenThrow(InvalidUpdateApplianceTypeRqException.class);
        var rs = sut.updateApplianceType(updateApplianceTypeRq);
        assertFalse(rs.getBody().isSuccess());
    }

    private void withValidCreateTypeRq() {
        createApplianceTypeRq = new CreateApplianceTypeRq(TYPE_NAME);
    }

    private void withValidUpdateTypeRq() {
        updateApplianceTypeRq = new UpdateApplianceTypeRq(TYPE_ID, TYPE_NAME);
    }

    private void withValidSelectTypeRq() {
        selectApplianceTypeRq = new SelectApplianceTypeRq(TYPE_NAME);
    }

    private void withInvalidCreateRq() {
        createApplianceTypeRq = new CreateApplianceTypeRq(null);
    }

    private void withInvalidUpdateRq() {
        updateApplianceTypeRq = new UpdateApplianceTypeRq(null, null);
    }

    private UpdateApplianceTypeRs getUpdateApplianceTypeRs() {
        return new UpdateApplianceTypeRs(true, RS_MESSAGE);
    }

    private SelectApplianceTypeRs getSelectApplianceTypeRs() {
        return new SelectApplianceTypeRs(true, null);
    }
}