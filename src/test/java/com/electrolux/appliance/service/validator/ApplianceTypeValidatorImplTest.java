package com.electrolux.appliance.service.validator;

import com.electrolux.appliance.dto.appliancetype.CreateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRq;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceTypeRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceTypeRqException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ApplianceTypeValidatorImplTest {

    private static String TYPE_NAME = "typename";
    private static final Long TYPE_ID = 1L;

    private ApplianceTypeValidatorImpl sut = new ApplianceTypeValidatorImpl();

    @ParameterizedTest
    @MethodSource("getInvalidCreateRq")
    public void shouldThrowInvalidException_whenInvalidCreateRq(CreateApplianceTypeRq rq) {
        Exception ex = assertThrows(InvalidCreateApplianceTypeRqException.class, () -> sut.valid(rq));
        assertNotNull(ex);
    }

    @ParameterizedTest
    @MethodSource("getInvalidUpdateRq")
    public void shouldThrowInvalidException_whenInvalidUpdateRq(UpdateApplianceTypeRq rq) {
        Exception ex = assertThrows(InvalidUpdateApplianceTypeRqException.class, () -> sut.valid(rq));
        assertNotNull(ex);
    }

    @Test
    public void shouldValidCreateRq() {
        sut.valid(getValidCreateRq());
    }

    @Test
    public void shouldValidUpdateRq() {
        sut.valid(getValidUpdateRq());
    }

    private static Stream<Arguments> getInvalidCreateRq() {
        return Stream.of(
                Arguments.of(new CreateApplianceTypeRq(null)),
                Arguments.of(new CreateApplianceTypeRq(""))
        );
    }

    private static Stream<Arguments> getInvalidUpdateRq() {
        return Stream.of(
                Arguments.of(new UpdateApplianceTypeRq(null, TYPE_NAME)),
                Arguments.of(new UpdateApplianceTypeRq(TYPE_ID, null)),
                Arguments.of(new UpdateApplianceTypeRq(TYPE_ID, ""))
        );
    }

    private CreateApplianceTypeRq getValidCreateRq() {
        return new CreateApplianceTypeRq(TYPE_NAME);
    }

    private UpdateApplianceTypeRq getValidUpdateRq() {
        return new UpdateApplianceTypeRq(TYPE_ID, TYPE_NAME);
    }
}