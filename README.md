Пока еще не реализовано удаление данных из таблиц. Тесты на *Dao.class тоже пока отсутствуют.

Архитектурно делал в расчете на дальнейшее развитие и усложнение модуля.
Есть уязвимые места в плане работы с БД, для их разрешения нужно лучше продумывать и прорабатывать взаимодействие с ней.
В этом приложении больше упор делал на саму архитектуру.

Описание таблиц и схемы находятся в файле src/test/resources/db/db_init.sql .
Есть две таблицы appliance и appliance_type.
Для старта базы использовал docker.

Примеры запросов для appliance
1 Вставка значения. POST
URL: localhost:8080/create/appliance
Пример тела запроса JSON
{
    "applianceName": "indesit z10",
    "price": 100,
    "typeId": 3
}
Все поля обязательные.
Ответ если удачно:
{
    "success": true,
    "body": {
        "applianceId": 20
    }
}
Где в "body" указано appliance_id новой записи

Здесь и далее при ошибке валидации запроса(нет полей) придет ответ вида
{
    "success": false,
    "cause": "Invalid parameters to select appliance."
}
Будут меняться текстовки "cause" в зависимости от операции

2 Обновление. POST
URL: localhost:8080/update/appliance
Пример тела запроса JSON
{
    "applianceId": 5,
    "applianceName": "indesit z10",
    "price": 100,
    "typeId": 3
}
Все поля обязательные.

Удачный ответ
{
    "success": true,
    "body": "Update appliance success!"
}


3.1 Выборка по applianceId. POST
URL: localhost:8080/select/appliance/byId
ПРимер запроса
{
    "applianceIds": [
       16
    ]
}
Поле applianceIds обязательно
Пример успешного запроса 
{
    "success": true,
    "body": [
        {
            "applianceId": 16,
            "applianceName": "indesit z10",
            "price": 100.000000000,
            "typeId": 1
        }
    ]
}
Если ничего не нашел, то поле "body" будет пустым.

3.2 Выборка по ключевым полям. POST
URL: localhost:8080/select/appliance
Пример запроса
{
    "applianceName": "indesit z10",
    "price": 100,
    "typeId": 1
}
Все поля обязательные
Успешный ответ. Их так много ибо в процессе написания активно слал запросы.
{
    "success": true,
    "body": [
        {
            "applianceId": 5,
            "applianceName": "indesit z10",
            "price": 100.000000000,
            "typeId": 1
        },
        {
            "applianceId": 13,
            "applianceName": "indesit z10",
            "price": 100.000000000,
            "typeId": 1
        },
        {
            "applianceId": 15,
            "applianceName": "indesit z10",
            "price": 100.000000000,
            "typeId": 1
        },
        {
            "applianceId": 16,
            "applianceName": "indesit z10",
            "price": 100.000000000,
            "typeId": 1
        }
    ]
}
Если ничего не найдено, "body" будет пустым.




Примеры запросов для appliance_type
1 Вставка записи. POST
URL: localhost:8080/create/applianceType
{
    "typeName": "synchrophasotron"
}
Поле обязательное
Пример ответа
{
    "success": true,
    "body": {
        "typeId": 10
    }
}
Где в "body" указан type_id записи


2 Изменение записи. POST
URL: localhost:8080/update/applianceType
{
    "typeId": 1,
    "typeName": "desintegrator"
}
Все поля обязательные

Удачный ответ
{
    "success": true,
    "body": "Update appliance success!"
}

3 Выборка. POST
URL: localhost:8080/select/applianceType
{
    "typeName": "desintegrator"
}
Поле обязательное

Успешный ответ 
{
    "success": true,
    "body": [
        {
            "typeId": 1,
            "typeName": "desintegrator"
        }
    ]
}