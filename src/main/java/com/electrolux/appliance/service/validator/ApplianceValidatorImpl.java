package com.electrolux.appliance.service.validator;

import com.electrolux.appliance.dto.appliance.CreateApplianceRq;
import com.electrolux.appliance.dto.appliance.SelectApplianceByIdRq;
import com.electrolux.appliance.dto.appliance.SelectApplianceRq;
import com.electrolux.appliance.dto.appliance.UpdateApplianceRq;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidSelectApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceRqException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import static org.springframework.util.StringUtils.isEmpty;

@Component
public class ApplianceValidatorImpl implements ApplianceValidator {

    private static final String INVALID_CREATE_APPLIANCE_MESSAGE = "Invalid parameters to create new appliance.";
    private static final String INVALID_UPDATE_APPLIANCE_MESSAGE = "Invalid parameters to update appliance.";
    private static final String INVALID_SELECT_APPLIANCE_MESSAGE = "Invalid parameters to select appliance.";

    @Override
    public void valid(CreateApplianceRq rq) {
        if (isEmpty(rq.getApplianceName())
                || rq.getPrice() == null
                || rq.getTypeId() == null) {
            throw new InvalidCreateApplianceRqException(INVALID_CREATE_APPLIANCE_MESSAGE);
        }
    }

    @Override
    public void valid(UpdateApplianceRq rq) {
        if (isEmpty(rq.getApplianceName())
                || rq.getApplianceId() == null
                || rq.getPrice() == null
                || rq.getTypeId() == null) {
            throw new InvalidUpdateApplianceRqException(INVALID_UPDATE_APPLIANCE_MESSAGE);
        }
    }

    @Override
    public void valid(SelectApplianceRq rq) {
        if (isEmpty(rq.getApplianceName())
                || rq.getPrice() == null
                || rq.getTypeId() == null) {
            throw new InvalidSelectApplianceRqException(INVALID_SELECT_APPLIANCE_MESSAGE);
        }
    }

    @Override
    public void valid(SelectApplianceByIdRq rq) {
        if (CollectionUtils.isEmpty(rq.getApplianceIds())) {
            throw new InvalidSelectApplianceRqException(INVALID_SELECT_APPLIANCE_MESSAGE);
        }
    }
}
