package com.electrolux.appliance.service.impl;

import com.electrolux.appliance.dao.ApplianceDao;
import com.electrolux.appliance.dto.appliance.*;
import com.electrolux.appliance.service.CrudApplianceService;
import com.electrolux.appliance.service.validator.ApplianceValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CrudApplianceServiceImpl implements CrudApplianceService {

    private final ApplianceValidator validator;
    private final ApplianceDao dao;

    @Override
    public CreateApplianceRs createAppliance(CreateApplianceRq rq) {
        validator.valid(rq);
        return new CreateApplianceRs(dao.createAppliance(rq));
    }

    @Override
    public UpdateApplianceRs updateApplience(UpdateApplianceRq rq) {
        validator.valid(rq);
        return dao.updateAppliance(rq);
    }

    @Override
    public SelectApplianceRs selectAppliance(SelectApplianceRq rq) {
        validator.valid(rq);
        return new SelectApplianceRs(true, dao.selectAppliance(rq));
    }

    @Override
    public SelectApplianceRs selectAppliance(SelectApplianceByIdRq rq) {
        validator.valid(rq);
        return new SelectApplianceRs(true, dao.selectAppliance(rq));
    }
}
