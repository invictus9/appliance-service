package com.electrolux.appliance.dao.mapper;

import com.electrolux.appliance.model.ApplianceTypeModel;
import jooq.tables.records.ApplianceTypeRecord;
import org.jooq.RecordMapper;
import org.springframework.stereotype.Component;

@Component
public class ApplianceTypeRecordMapper implements RecordMapper<ApplianceTypeRecord, ApplianceTypeModel> {
    @Override
    public ApplianceTypeModel map(ApplianceTypeRecord record) {
        return new ApplianceTypeModel(
                record.getTypeId(),
                record.getTypeName()
        );
    }
}
