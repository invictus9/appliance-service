package com.electrolux.appliance.dto.appliancetype;

import com.electrolux.appliance.model.ApplianceTypeModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class SelectApplianceTypeRs {

    private boolean success;
    private List<ApplianceTypeModel> appliances;
}
