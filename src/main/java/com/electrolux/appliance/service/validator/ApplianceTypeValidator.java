package com.electrolux.appliance.service.validator;

import com.electrolux.appliance.dto.appliancetype.CreateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.SelectApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRq;

public interface ApplianceTypeValidator {

    void valid(CreateApplianceTypeRq rq);

    void valid(UpdateApplianceTypeRq rq);

    void valid(SelectApplianceTypeRq rq);
}
