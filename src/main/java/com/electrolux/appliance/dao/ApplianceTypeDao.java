package com.electrolux.appliance.dao;

import com.electrolux.appliance.dto.appliancetype.CreateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.SelectApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRs;
import com.electrolux.appliance.model.ApplianceTypeModel;

import java.util.List;

public interface ApplianceTypeDao {

    Long createApplianceType(CreateApplianceTypeRq rq);

    UpdateApplianceTypeRs updateApplianceType(UpdateApplianceTypeRq rq);

    List<ApplianceTypeModel> selectApplianceType(SelectApplianceTypeRq rq);
}
