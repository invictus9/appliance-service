package com.electrolux.appliance.dto.appliance;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CreateApplianceRs {

    private Long applianceId;
}
