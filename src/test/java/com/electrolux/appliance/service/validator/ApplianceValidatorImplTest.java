package com.electrolux.appliance.service.validator;

import com.electrolux.appliance.dto.appliance.CreateApplianceRq;
import com.electrolux.appliance.dto.appliance.UpdateApplianceRq;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceRqException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ApplianceValidatorImplTest {

    private static final Long APPLIANCE_ID = 1L;
    private static final String APPLIANCE_NAME = "applianceName";
    private static final Double PRICE = 100D;
    private static final Long TYPE_ID = 1L;

    private ApplianceValidatorImpl sut = new ApplianceValidatorImpl();

    @ParameterizedTest
    @MethodSource("getInvalidCreateRq")
    public void shouldThrowInvalidException_whenInvalidCreateRq(CreateApplianceRq rq) {
        Exception ex = assertThrows(InvalidCreateApplianceRqException.class, () -> sut.valid(rq));
        assertNotNull(ex);
    }

    @ParameterizedTest
    @MethodSource("getInvalidUpdateRq")
    public void shouldThrowInvalidException_whenInvalidUpdateRq(UpdateApplianceRq rq) {
        Exception ex = assertThrows(InvalidUpdateApplianceRqException.class, () -> sut.valid(rq));
        assertNotNull(ex);
    }

    @Test
    public void shouldValidCreateRq() {
        sut.valid(getValidCreateRq());
    }

    @Test
    public void shouldValidUpdateRq() {
        sut.valid(getValidUpdateRq());
    }

    private static Stream<Arguments> getInvalidCreateRq() {
        return Stream.of(
                Arguments.of(new CreateApplianceRq(null, PRICE, TYPE_ID)),
                Arguments.of(new CreateApplianceRq("", PRICE, TYPE_ID)),
                Arguments.of(new CreateApplianceRq(APPLIANCE_NAME, null, TYPE_ID)),
                Arguments.of(new CreateApplianceRq(APPLIANCE_NAME, PRICE, null))
        );
    }

    private static Stream<Arguments> getInvalidUpdateRq() {
        return Stream.of(
                Arguments.of(new UpdateApplianceRq(null, APPLIANCE_NAME, PRICE, TYPE_ID)),
                Arguments.of(new UpdateApplianceRq(APPLIANCE_ID, null, PRICE, TYPE_ID)),
                Arguments.of(new UpdateApplianceRq(APPLIANCE_ID, "", PRICE, TYPE_ID)),
                Arguments.of(new UpdateApplianceRq(APPLIANCE_ID, APPLIANCE_NAME, null, TYPE_ID)),
                Arguments.of(new UpdateApplianceRq(APPLIANCE_ID, APPLIANCE_NAME, PRICE, null))
        );
    }

    private CreateApplianceRq getValidCreateRq() {
        return new CreateApplianceRq(APPLIANCE_NAME, PRICE, TYPE_ID);
    }

    private UpdateApplianceRq getValidUpdateRq() {
        return new UpdateApplianceRq(APPLIANCE_ID, APPLIANCE_NAME, PRICE, TYPE_ID);
    }
}