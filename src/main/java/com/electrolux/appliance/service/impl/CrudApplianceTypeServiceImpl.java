package com.electrolux.appliance.service.impl;

import com.electrolux.appliance.dao.ApplianceTypeDao;
import com.electrolux.appliance.dto.appliancetype.*;
import com.electrolux.appliance.service.CrudApplianceTypeService;
import com.electrolux.appliance.service.validator.ApplianceTypeValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CrudApplianceTypeServiceImpl implements CrudApplianceTypeService {

    private final ApplianceTypeValidator validator;
    private final ApplianceTypeDao dao;

    @Override
    public CreateApplianceTypeRs createApplianceType(CreateApplianceTypeRq rq) {
        validator.valid(rq);
        return new CreateApplianceTypeRs(dao.createApplianceType(rq));
    }

    @Override
    public UpdateApplianceTypeRs updateApplianceType(UpdateApplianceTypeRq rq) {
        validator.valid(rq);
        return dao.updateApplianceType(rq);
    }

    @Override
    public SelectApplianceTypeRs selectApplianceType(SelectApplianceTypeRq rq) {
        validator.valid(rq);
        return new SelectApplianceTypeRs(true, dao.selectApplianceType(rq));
    }
}
