package com.electrolux.appliance.dto.appliancetype;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UpdateApplianceTypeRs {

    private boolean success;
    private String message;
}
