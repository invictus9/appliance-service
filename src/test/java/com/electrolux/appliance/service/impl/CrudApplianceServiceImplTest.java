package com.electrolux.appliance.service.impl;

import com.electrolux.appliance.dao.ApplianceDao;
import com.electrolux.appliance.dto.appliance.CreateApplianceRq;
import com.electrolux.appliance.dto.appliance.UpdateApplianceRq;
import com.electrolux.appliance.service.validator.ApplianceValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class CrudApplianceServiceImplTest {

    private static final Long APPLIANCE_ID = 1L;
    private static final String APPLIANCE_NAME = "applianceName";
    private static final Double PRICE = 100D;
    private static final Long TYPE_ID = 1L;

    @Mock
    private ApplianceValidator validator;
    @Mock
    private ApplianceDao dao;

    private CreateApplianceRq createRq = getValidCreateRq();
    private UpdateApplianceRq updateRq = getValidUpdateRq();

    private CrudApplianceServiceImpl sut;

    @BeforeEach
    void setUp() {
        initMocks(this);
        sut = new CrudApplianceServiceImpl(validator, dao);
    }

    @Test
    public void shouldCallValidator_onCreateAppliance() {
        sut.createAppliance(createRq);
        verify(validator).valid(createRq);
    }

    @Test
    public void shouldCallValidator_onUpdateAppliance() {
        sut.updateApplience(updateRq);
        verify(validator).valid(updateRq);
    }

    @Test
    public void shouldCallDao_onCreateAppliance() {
        sut.createAppliance(createRq);
        verify(dao).createAppliance(createRq);
    }

    @Test
    public void shouldCallDao_onUpdateAppliance() {
        sut.updateApplience(updateRq);
        verify(dao).updateAppliance(updateRq);
    }

    private CreateApplianceRq getValidCreateRq() {
        return new CreateApplianceRq(APPLIANCE_NAME, PRICE, TYPE_ID);
    }

    private UpdateApplianceRq getValidUpdateRq() {
        return new UpdateApplianceRq(APPLIANCE_ID, APPLIANCE_NAME, PRICE, TYPE_ID);
    }
}