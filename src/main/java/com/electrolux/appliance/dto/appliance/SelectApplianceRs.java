package com.electrolux.appliance.dto.appliance;

import com.electrolux.appliance.model.ApplianceModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class SelectApplianceRs {

    private boolean success;
    private List<ApplianceModel> appliances;
}
