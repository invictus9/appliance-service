package com.electrolux.appliance.service.impl;

import com.electrolux.appliance.dao.ApplianceTypeDao;
import com.electrolux.appliance.dto.appliancetype.CreateApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.SelectApplianceTypeRq;
import com.electrolux.appliance.dto.appliancetype.UpdateApplianceTypeRq;
import com.electrolux.appliance.service.validator.ApplianceTypeValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class CrudApplianceTypeServiceImplTest {

    private static String TYPE_NAME = "typename";
    private static final Long TYPE_ID = 1L;

    @Mock
    private ApplianceTypeValidator validator;
    @Mock
    private ApplianceTypeDao dao;

    private CreateApplianceTypeRq createRq = getValidCreateRq();
    private UpdateApplianceTypeRq updateRq = getValidUpdateRq();
    private SelectApplianceTypeRq selectRq = getValidSelectRq();

    private CrudApplianceTypeServiceImpl sut;

    @BeforeEach
    void setUp() {
        initMocks(this);
        sut = new CrudApplianceTypeServiceImpl(validator, dao);
    }

    @Test
    public void shouldCallValidator_onCreateAppliance() {
        sut.createApplianceType(createRq);
        verify(validator).valid(createRq);
    }

    @Test
    public void shouldCallValidator_onUpdateAppliance() {
        sut.updateApplianceType(updateRq);
        verify(validator).valid(updateRq);
    }

    @Test
    public void shouldCallValidator_onSelectAppliance() {
        sut.selectApplianceType(selectRq);
        verify(validator).valid(selectRq);
    }

    @Test
    public void shouldCallDao_onCreateAppliance() {
        sut.createApplianceType(createRq);
        verify(dao).createApplianceType(createRq);
    }

    @Test
    public void shouldCallDao_onUpdateAppliance() {
        sut.updateApplianceType(updateRq);
        verify(dao).updateApplianceType(updateRq);
    }

    @Test
    public void shouldCallDao_onSelectAppliance() {
        sut.selectApplianceType(selectRq);
        verify(dao).selectApplianceType(selectRq);
    }

    private CreateApplianceTypeRq getValidCreateRq() {
        return new CreateApplianceTypeRq(TYPE_NAME);
    }

    private UpdateApplianceTypeRq getValidUpdateRq() {
        return new UpdateApplianceTypeRq(TYPE_ID, TYPE_NAME);
    }

    private SelectApplianceTypeRq getValidSelectRq() {
        return new SelectApplianceTypeRq(TYPE_NAME);
    }
}