package com.electrolux.appliance.service;

import com.electrolux.appliance.dto.appliance.*;

public interface CrudApplianceService {

    CreateApplianceRs createAppliance(CreateApplianceRq rq);

    UpdateApplianceRs updateApplience(UpdateApplianceRq rq);

    SelectApplianceRs selectAppliance(SelectApplianceRq rq);

    SelectApplianceRs selectAppliance(SelectApplianceByIdRq rq);
}
