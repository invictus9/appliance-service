package com.electrolux.appliance.controller;

import com.electrolux.appliance.dto.BaseResponse;
import com.electrolux.appliance.dto.ErrorResponse;
import com.electrolux.appliance.dto.SuccessResponse;
import com.electrolux.appliance.dto.appliance.*;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidSelectApplianceRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceRqException;
import com.electrolux.appliance.service.CrudApplianceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ApplianceController {

    private static final String SELECT_APPLIANCE_FAILED = "Select appliance failed";

    private final CrudApplianceService applianceService;

    @Autowired
    public ApplianceController(CrudApplianceService applianceService) {
        this.applianceService = applianceService;
    }

    @PostMapping(value = "${rest.create.appliance}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> createAppliance(@RequestBody CreateApplianceRq rq) {
        try {
            CreateApplianceRs rs = applianceService.createAppliance(rq);
            return new ResponseEntity<>(new SuccessResponse<>(true, rs), HttpStatus.OK);
        } catch (InvalidCreateApplianceRqException ex) {
            log.error("Create appliance failed", ex);
            return badResponse(ex);
        }
    }

    @PostMapping(value = "${rest.update.appliance}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> updateAppliance(@RequestBody UpdateApplianceRq rq) {
        try {
            UpdateApplianceRs rs = applianceService.updateApplience(rq);
            return new ResponseEntity(new SuccessResponse(rs.isSuccess(), rs.getMessage()), HttpStatus.OK);
        } catch (InvalidUpdateApplianceRqException ex) {
            log.error("Update appliance failed", ex);
            return badResponse(ex);
        }
    }

    @PostMapping(value = "${rest.select.appliance}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> selectAppliances(@RequestBody SelectApplianceRq rq) {
        try {
            SelectApplianceRs rs = applianceService.selectAppliance(rq);
            return new ResponseEntity<>(new SuccessResponse(rs.isSuccess(), rs.getAppliances()), HttpStatus.OK);
        } catch (InvalidSelectApplianceRqException ex) {
            log.error(SELECT_APPLIANCE_FAILED, ex);
            return badResponse(ex);
        }
    }

    @PostMapping(value = "${rest.select.applianceById}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> selectAppliances(@RequestBody SelectApplianceByIdRq rq) {
        try {
            SelectApplianceRs rs = applianceService.selectAppliance(rq);
            return new ResponseEntity<>(new SuccessResponse(rs.isSuccess(), rs.getAppliances()), HttpStatus.OK);
        } catch (InvalidSelectApplianceRqException ex) {
            log.error(SELECT_APPLIANCE_FAILED, ex);
            return badResponse(ex);
        }
    }

    private ResponseEntity<ErrorResponse> badResponse(Exception ex) {
        return new ResponseEntity<>(new ErrorResponse(false, ex.getMessage()), HttpStatus.OK);
    }
}
