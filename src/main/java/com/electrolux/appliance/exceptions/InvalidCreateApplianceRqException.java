package com.electrolux.appliance.exceptions;

public class InvalidCreateApplianceRqException extends RuntimeException {

    public InvalidCreateApplianceRqException(String message) {
        super(message);
    }
}
