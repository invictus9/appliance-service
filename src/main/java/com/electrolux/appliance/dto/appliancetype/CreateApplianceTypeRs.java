package com.electrolux.appliance.dto.appliancetype;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CreateApplianceTypeRs {

    private Long typeId;
}
