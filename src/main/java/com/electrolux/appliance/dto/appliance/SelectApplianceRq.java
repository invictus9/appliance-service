package com.electrolux.appliance.dto.appliance;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SelectApplianceRq {

    private String applianceName;
    private Double price;
    private Long typeId;
}
