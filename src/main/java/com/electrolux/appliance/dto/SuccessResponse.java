package com.electrolux.appliance.dto;

import lombok.Getter;

@Getter
public class SuccessResponse<T> extends BaseResponse {

    private final T body;

    public SuccessResponse(boolean success, T body) {
        super(success);
        this.body = body;
    }
}
