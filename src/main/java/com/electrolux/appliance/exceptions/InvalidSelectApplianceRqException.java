package com.electrolux.appliance.exceptions;

public class InvalidSelectApplianceRqException extends RuntimeException {

    public InvalidSelectApplianceRqException(String message) {
        super(message);
    }
}
