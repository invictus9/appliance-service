package com.electrolux.appliance.dao.mapper;

import com.electrolux.appliance.model.ApplianceModel;
import jooq.tables.records.ApplianceRecord;
import org.jooq.RecordMapper;
import org.springframework.stereotype.Component;

@Component
public class ApplianceRecordMapper implements RecordMapper<ApplianceRecord, ApplianceModel> {
    @Override
    public ApplianceModel map(ApplianceRecord record) {
        return new ApplianceModel(
                record.getApplianceId(),
                record.getApplianceName(),
                record.getPrice(),
                record.getTypeId()
        );
    }
}
