package com.electrolux.appliance.dao.creators;

import com.electrolux.appliance.dto.appliance.SelectApplianceByIdRq;
import com.electrolux.appliance.dto.appliance.SelectApplianceRq;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static jooq.tables.Appliance.APPLIANCE;
import static org.springframework.util.StringUtils.isEmpty;

@Component
public class SelectConditionCreator {

    public String createConditionWhere(SelectApplianceRq rq) {
        StringBuilder sql = new StringBuilder();
        if (!isEmpty(rq.getApplianceName())) {
            sql.append(APPLIANCE.APPLIANCE_NAME.getName() + " = " + "'" + rq.getApplianceName() + "'");
        }
        if (rq.getPrice() != null) {
            sql.append(" and " + APPLIANCE.PRICE.getName() + " = " + new BigDecimal(rq.getPrice()));
        }
        if (rq.getTypeId() != null) {
            sql.append(" and " + APPLIANCE.TYPE_ID + " = " + rq.getTypeId());
        }
        return sql.toString();
    }

    public String createConditionIdIn(SelectApplianceByIdRq rq) {
        StringBuilder sql = new StringBuilder();
        sql.append(APPLIANCE.APPLIANCE_ID.getName() + " in (");
        rq.getApplianceIds().stream().forEach(id -> sql.append(id + ","));
        sql.deleteCharAt(sql.length() - 1);//delete the last comma
        sql.append(")");
        return sql.toString();
    }
}
