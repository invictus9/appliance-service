package com.electrolux.appliance.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApplianceTypeModel {

    private Long typeId;
    private String typeName;
}
