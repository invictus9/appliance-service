package com.electrolux.appliance.service.validator;

import com.electrolux.appliance.dto.appliance.CreateApplianceRq;
import com.electrolux.appliance.dto.appliance.SelectApplianceByIdRq;
import com.electrolux.appliance.dto.appliance.SelectApplianceRq;
import com.electrolux.appliance.dto.appliance.UpdateApplianceRq;

public interface ApplianceValidator {

    void valid(CreateApplianceRq rq);

    void valid(UpdateApplianceRq rq);

    void valid(SelectApplianceRq rq);

    void valid(SelectApplianceByIdRq rq);
}
