package com.electrolux.appliance.exceptions;

public class InvalidUpdateApplianceRqException extends RuntimeException {

    public InvalidUpdateApplianceRqException(String message) {
        super(message);
    }
}
