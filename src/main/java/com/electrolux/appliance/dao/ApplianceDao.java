package com.electrolux.appliance.dao;

import com.electrolux.appliance.dto.appliance.*;
import com.electrolux.appliance.model.ApplianceModel;

import java.util.List;

public interface ApplianceDao {

    Long createAppliance(CreateApplianceRq rq);

    UpdateApplianceRs updateAppliance(UpdateApplianceRq rq);

    List<ApplianceModel> selectAppliance(SelectApplianceRq rq);

    List<ApplianceModel> selectAppliance(SelectApplianceByIdRq rq);
}
