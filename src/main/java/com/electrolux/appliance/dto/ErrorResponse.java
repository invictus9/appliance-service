package com.electrolux.appliance.dto;

import lombok.Getter;

@Getter
public class ErrorResponse extends BaseResponse {

    private final String cause;

    public ErrorResponse(boolean success, String cause) {
        super(success);
        this.cause = cause;
    }
}
