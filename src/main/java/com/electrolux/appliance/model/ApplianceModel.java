package com.electrolux.appliance.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class ApplianceModel {

    private Long applianceId;
    private String applianceName;
    private BigDecimal price;
    private Long typeId;
}
