package com.electrolux.appliance.dto.appliance;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SelectApplianceByIdRq {

    private List<Long> applianceIds;
}
