drop schema if EXISTS appliance_service;
create schema appliance_service;

drop table if EXISTS appliance_service.appliance_type;
create table appliance_service.appliance_type (
    type_id bigserial PRIMARY KEY,
    type_name varchar(50) NOT NULL
);

drop table if EXISTS appliance_service.appliance;
create table appliance_service.appliance (
    appliance_id bigserial PRIMARY KEY,
    type_id bigint REFERENCES appliance_service.appliance_type(type_id) NOT NULL,
    appliance_name VARCHAR(100) NOT NULL,
    price NUMERIC(18,9) NOT NULL
);

insert into appliance_service.appliance_type values (1, 'oven');
insert into appliance_service.appliance_type values (2, 'wash machine');
commit;