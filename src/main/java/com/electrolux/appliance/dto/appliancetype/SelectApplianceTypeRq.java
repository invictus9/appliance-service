package com.electrolux.appliance.dto.appliancetype;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SelectApplianceTypeRq {

    private String typeName;
}
