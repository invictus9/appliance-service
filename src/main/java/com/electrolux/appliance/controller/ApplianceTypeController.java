package com.electrolux.appliance.controller;

import com.electrolux.appliance.dto.BaseResponse;
import com.electrolux.appliance.dto.ErrorResponse;
import com.electrolux.appliance.dto.SuccessResponse;
import com.electrolux.appliance.dto.appliancetype.*;
import com.electrolux.appliance.exceptions.InvalidCreateApplianceTypeRqException;
import com.electrolux.appliance.exceptions.InvalidSelectApplianceTypeRqException;
import com.electrolux.appliance.exceptions.InvalidUpdateApplianceTypeRqException;
import com.electrolux.appliance.service.CrudApplianceTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ApplianceTypeController {

    private final CrudApplianceTypeService applianceTypeService;

    @Autowired
    public ApplianceTypeController(CrudApplianceTypeService applianceTypeService) {
        this.applianceTypeService = applianceTypeService;
    }

    @PostMapping(value = "${rest.create.applianceType}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> createApplianceType(@RequestBody CreateApplianceTypeRq rq) {
        try {
            CreateApplianceTypeRs rs = applianceTypeService.createApplianceType(rq);
            return new ResponseEntity(new SuccessResponse<>(true, rs), HttpStatus.OK);
        } catch (InvalidCreateApplianceTypeRqException ex) {
            log.error("Create appliance type failed", ex);
            return badResponse(ex);
        }
    }

    @PostMapping(value = "${rest.update.applianceType}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> updateApplianceType(@RequestBody UpdateApplianceTypeRq rq) {
        try {
            UpdateApplianceTypeRs rs = applianceTypeService.updateApplianceType(rq);
            return new ResponseEntity(new SuccessResponse(rs.isSuccess(), rs.getMessage()), HttpStatus.OK);
        } catch (InvalidUpdateApplianceTypeRqException ex) {
            log.error("Update appliance type failed", ex);
            return badResponse(ex);
        }
    }

    @PostMapping(value = "${rest.select.applianceType}", produces = "application/json")
    public ResponseEntity<? extends BaseResponse> selectApplianceType(@RequestBody SelectApplianceTypeRq rq) {
        try {
            SelectApplianceTypeRs rs = applianceTypeService.selectApplianceType(rq);
            return new ResponseEntity(new SuccessResponse(rs.isSuccess(), rs.getAppliances()), HttpStatus.OK);
        } catch (InvalidSelectApplianceTypeRqException ex) {
            return badResponse(ex);
        }
    }

    private ResponseEntity<ErrorResponse> badResponse(Exception ex) {
        return new ResponseEntity<>(new ErrorResponse(false, ex.getMessage()), HttpStatus.OK);
    }
}
