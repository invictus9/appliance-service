package com.electrolux.appliance.exceptions;

public class InvalidCreateApplianceTypeRqException extends RuntimeException {

    public InvalidCreateApplianceTypeRqException(String message) {
        super(message);
    }
}
