package com.electrolux.appliance.dto.appliance;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UpdateApplianceRs {

    private boolean success;
    private String message;
}
